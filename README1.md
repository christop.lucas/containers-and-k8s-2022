# Requête
https://ps-chris.kube.isc.heia-fr.ch/
https://ps-chris.kube.isc.heia-fr.ch/select
https://ps-chris.kube.isc.heia-fr.ch/select2?dataid=2
https://ps-chris.kube.isc.heia-fr.ch/select3?datetime=2021-04-14T13:15:33.000Z
# Problèmes rencontrés
- Je ne peux pas tourner le pod contenant le nodejs localement. (ni minikube, ni kubectl). Liée ingress?
- Connexion entre le nodejs et la mariadb
- Gitlab Container Registry ne se mettait pas à jour après un nouveau build de l'image
- Helm ne lance pas le ingress ni le service
- 502 Bad Gateway nodejs 
# Améliorations
- Ajouter un php-myadmin et faire fonctionner Helm
- Utiliser du Multi-stage
# Conclusion
- Parti docker facile
- Parti kubernetes était un enfer
- Utilisation de nos projet
- Plus de temps = meilleur résultat