DROP DATABASE IF EXISTS test;
CREATE DATABASE test;
USE test;
CREATE TABLE Data (
    dataid int NOT NULL AUTO_INCREMENT,
    name varchar(50),
    datatime datetime,
    PRIMARY KEY (Dataid)
);
INSERT INTO Data (name, datatime) VALUES ('ABC', '1019-01-01 00:00:00');
INSERT INTO Data (name, datatime) VALUES ('DEF', '2020-01-01 00:00:00');