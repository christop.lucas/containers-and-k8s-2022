<?php
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
    
    protected $clients;
    private $types = array("CreateRDV", "UpdateRDV", "DeleteRDV",
                            "CreateSeance", "ValideSeance", "DeleteSeance",
                            "CreateClient", "UpdateClient", "DeleteClient"
                        );


    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    
    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        // Check the message before sending it on to the others        
        if ($this->checkMessage($msg)) {
            foreach ($this->clients as $client) {
                if ($from !== $client) {
                    // The sender is not the receiver, send to each client connected
                    $client->send($msg);
                }
            }
        } else {
            foreach ($this->clients as $client) {
                if ($from === $client) {
                    $client->send("ERROR : This is not acceptable ! Refer to guidelines.");
                    break;
                }
            }
            return;
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }

    private function checkMessage($msg) {
        $result = false;
        $obj = $this->isJSON($msg);
        if ($obj != null) {
            $ids = $obj->{'ids'};
            $type = $obj->{'type'};
            // is_string and is_array checks for null and sends false if the case
            if (is_array($ids) && is_string($type)){
                if(in_array($type, $types, true)) {
                    $result = array_filter($ids, 'is_int');
                }
            }
        }
        return $result;
    }

    private function isJSON($msg) {
        if(is_string($msg)) {
            $obj = json_decode($msg);
            if (json_last_error() == JSON_ERROR_NONE) {
                return $obj;
            }
        }
        return null;
    }
}

?>