<?php
//These are the defined authentication environment in the db service
require('/var/www/html/vendor/autoload.php');
require_once('/var/www/html/websocket.php');
// The MySQL service named in the docker-compose.yml.
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Chat()
        )
    ),8080
);
$server->run();
?>