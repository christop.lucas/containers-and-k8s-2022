'use strict';
var bodyParser = require("body-parser");
const express = require('express');
const mariadb = require('mariadb');
//DB
var pool = mariadb.createPool({
  host     : 'ttsyncdb',
  user     : 'root',
  password : 'test',
  database : 'test'
});
// Constants
const PORT = 8085;
const HOST = '0.0.0.0';

// App
const app = express();
// JSON
app.use(bodyParser.json());
// REQUEST
app.get('/', (req, res) => {
    res.send('Hello World');
});

app.get('/test', (req, res) => {
  res.send('Test');
});

app.get('/select', async (req, res) => {
    let connection;
    try {
        connection = await pool.getConnection();
        console.log("select");
        var rows = await connection.query("SELECT * from Data;");
        delete rows['meta'];
        res.send(rows);
        console.log(rows);
    } catch (error) {
        console.log(error);
    } finally {
        if (connection) {
        connection.release();
        }
    }
});

app.get('/select2', async (req, res) => {
  let connection;
  try {
      connection = await pool.getConnection();
      console.log("select2");
      let data = req.query.dataid;
      console.log("DATA: "+data)
      if (Number(data)) {
        var rows = await connection.query("SELECT dataid,name from Data where dataid = ?",data);
        delete rows['meta'];
        res.send(rows);
        console.log(rows);
      }
      
  } catch (error) {
      console.log(error);
  } finally {
      if (connection) {
      connection.release();
      }
  }
});

app.get('/select3', async (req, res) => {
  let connection;
  try {
      connection = await pool.getConnection();
      console.log("select3");
      let data = req.query.datetime;
      console.log("DATA: "+data)
      if (String(data)) {
        var rows = await connection.query("SELECT dataid,name from Data where datatime > ?",data);
        delete rows['meta'];
        res.send(rows);
        console.log(rows);
      }
      
  } catch (error) {
      console.log(error);
  } finally {
      if (connection) {
      connection.release();
      }
  }
});

app.post('/add', async (req, res) => {
    console.log("Start Add");
    let connection;
    try {
        connection = await pool.getConnection();
        await connection.beginTransaction(); // Start Transaction
        try {
          // Create conference
          let data = req.body;
          console.log(data);
          console.log(data.datatime);
          const result = await pool.query("INSERT INTO `Data` (`name`,`datatime`) VALUES (?, ?)", [
            data.name,
            data.datatime
          ]);
          await connection.commit();// Commit Changes
          console.log(result);
          res.send({"result":"SUCCESS", "id": Number(result.insertId)});
        } catch (err) {
          console.error("Error on request, reverting changes: ", err);
          await connection.rollback();// Rollback Changes
          res.send({"result":"FAILED", "id": -1});
        }
      } catch (err) {
        console.error("Error starting a transaction", err);
      }
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);