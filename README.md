# Technology test - Synchronisation (WebSocket)

## Why ?

This test is to figure out if using WebSockets will solve TurboPhysio mobile synchronisation problems. At the moment, the mobile apps synchronise at the launch of the application and manually (option in settings).

## Goal

The first goal is to realise an infrastructure with a database and a webserver with a PHP WebSocket. Once that is in place, create a multiple clients to test if they can connect to the WebSocket to receive informations.

## What is a WebSocket ?

> WebSocket is a communication protocol, providing a bidirectional communnication (full-duplex communication channnels) over a single TCP connection. This protocol enables interaction between a web browser (or other applicaiton)  and a web server with lower overhead than half-duplex alternatives such as HTTP polling, facilitating real-time data transfer from and to the server.

## Why use a WebSocket ?

A WebSocket will allow us to implement a pub/sub system. When ever a device wants to write to the database, it sends a message to the WebSocket to write the information and notify the others.

## Schematics of the situation

![Schematics](schematics.jpeg)

## How to run test ?

Run the differents containers with the following command :

> docker compose up

Open multiple clients, either web or flutter. Sends messages and you should get the content of the table in the database.

## Librairies used

* [PHP WebSocket Server](http://socketo.me/)
* [Flutter WebSocket Client](https://pub.dev/packages/web_socket_channel)
* [JavaScript WebSocket Client](https://developer.mozilla.org/en-US/docs/Web/API/WebSocket)