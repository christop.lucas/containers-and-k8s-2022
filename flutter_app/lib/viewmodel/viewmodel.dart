import 'dart:convert';

import 'package:flutter/widgets.dart';
import '../model/message.dart';
import '../services/database/database.dart';
import '../services/repository.dart';

enum DataStatus { loading, success, failure }

class DataViewModel extends ChangeNotifier {
  List<int> ids = [];
  final Repository repository;
  var status = DataStatus.loading;

  DataViewModel({required this.repository}) {
    repository.channel.stream.listen((transit) {
      var data = json.decode(transit);
      var msg = Message.fromJson(data);
      fetchSpecificData(msg.ids);
    });
  }

  void fetchSpecificData(List<int> ids) async {
    await repository.fetchSpecificData(ids);
  }

  Stream<List<Data>> showData() {
    Stream<List<Data>> datas = repository.getData();
    status = DataStatus.success;
    return datas;
  }

  void fetchData() async {
    await repository.fetchData();
    status = DataStatus.success;
    notifyListeners();
  }

  void insertData(Data data) async {
    var res = await repository.insertData(data);
    if (res > 0) {
      ids.add(res);
      sendMessage(res);
      ids.clear();
      notifyListeners();
    }
  }

  void sendMessage(int id) {
    var message = Message(ids: [id], type: "DeleteSeance");
    repository.sendMessage(message);
  }
}
