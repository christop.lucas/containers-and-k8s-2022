import 'dart:convert';

import 'package:flutter_app/model/message.dart';
import 'package:flutter_app/services/database/database.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'api_helper.dart';

class Repository {
  final ApiHelper api = ApiHelper();
  final AppDb helper = AppDb();
  final channel = WebSocketChannel.connect(
    Uri.parse('ws://php-chris.kube.isc.heia-fr.ch'),
  );
  Repository();

  /*
  1) Fetch Data Online
  2) Store in Local database
  3) return stream
   */
  Future<void> fetchData() async {
    // Get instance shared preferences
    final shared = await SharedPreferences.getInstance();
    // Get the contents of local database;
    //var localDatas = await helper.getAllData();
    // The Datas that need to be written in database
    List<Data> changesDatas = [];
    //if (localDatas.isEmpty) {
    // Fetch all the content of online
    changesDatas.addAll(await api.getData());
    //} else {
    // Fetch the last time we fetched
    //  String datetime = shared.getString("lastReadData")!;
    //  print("Fetching missing data at " + datetime);
    //  changesDatas.addAll(await api.getMissingData(datetime));
    //}

    if (changesDatas.isNotEmpty) {
      // write locally
      helper.addAllData(changesDatas);
    }
    // Save the dateTime that we fetch all the information
    shared.setString("lastReadData", DateTime.now().toIso8601String());
  }

  Stream<List<Data>> getData() {
    return helper.watchEntriesInData();
  }

  Future<void> fetchSpecificData(List<int> ids) async {
    print("in repo getSpecificData");
    print(ids[0]);
    List<Data> result = [];
    for (var i = 0; i < ids.length; i++) {
      int index = ids[i];
      Data data = await api.getSpecificData(index);
      result.add(data);
    }
    helper.addAllData(result);
    final shared = await SharedPreferences.getInstance();
    String datetime = DateTime.now().toIso8601String();
    print(datetime);
    shared.setString("lastReadData", datetime);
  }

  Future<int> insertData(Data data) async {
    final res = await api.insertData(data);
    Data dataWithKey = Data(
      dataid: res,
      name: data.name,
    );
    helper.addData(dataWithKey);
    return res;
  }

  void sendMessage(Message message) {
    print("Sending a message");
    channel.sink.add(json.encode(message.toJson()));
  }
}
