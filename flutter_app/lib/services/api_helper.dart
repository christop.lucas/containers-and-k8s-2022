import 'dart:convert';
import 'package:flutter_app/model/insert_answer.dart';
import 'package:http/http.dart' as http;

import 'database/database.dart';

class DataRequestFailure implements Exception {}

class DataNotFoundFailure implements Exception {}

class ApiHelper {
  ApiHelper({http.Client? httpClient})
      : _httpClient = httpClient ?? http.Client();

  final http.Client _httpClient;
  static const _baseUrl = 'ps-chris.kube.isc.heia-fr.ch';

  //"https://v2.jokeapi.dev/joke/Any?type=single" &safe-mode
  //localhost:8585/select

  Future<List<Data>> getData() async {
    final dataRequest = Uri.https(
      _baseUrl,
      'select',
    );
    final dataResponse = await _httpClient.get(dataRequest);

    if (dataResponse.statusCode != 200) {
      throw DataRequestFailure();
    }
    final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
    if (dataJson.isEmpty) {
      throw DataNotFoundFailure();
    }
    return dataJson.map((e) => Data.fromJson(e)).toList();
  }

  Future<Data> getSpecificData(int id) async {
    final dataRequest = Uri.http(
      _baseUrl,
      'select2',
      {'dataid': id.toString()},
    );
    final dataResponse = await _httpClient.get(
      dataRequest,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (dataResponse.statusCode != 200) {
      throw DataRequestFailure();
    }
    final dataJson = jsonDecode(dataResponse.body);
    if (dataJson.isEmpty) {
      throw DataNotFoundFailure();
    }
    return dataJson.map((e) => Data.fromJson(e)).toList()[0];
  }

  Future<List<Data>> getMissingData(String dateTime) async {
    final dataRequest = Uri.http(
      _baseUrl,
      'select3',
      {'datetime': dateTime},
    );
    final dataResponse = await _httpClient.get(
      dataRequest,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (dataResponse.statusCode != 200) {
      throw DataRequestFailure();
    }
    final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
    return dataJson.map((e) => Data.fromJson(e)).toList();
  }

  Future<int> insertData(Data data) async {
    final dataRequest = Uri.http(
      _baseUrl,
      'add',
    );
    final dataResponse = await _httpClient.post(
      dataRequest,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'name': data.name,
        'datatime': DateTime.now().toIso8601String(),
      }),
    );
    if (dataResponse.statusCode != 200) {
      throw DataRequestFailure();
    }
    final dataJson = jsonDecode(dataResponse.body);
    var answer = InsertAnswer.fromJson(dataJson);
    if (answer.result == "FAILED") {
      throw DataNotFoundFailure();
    }
    return answer.id;
  }
}
