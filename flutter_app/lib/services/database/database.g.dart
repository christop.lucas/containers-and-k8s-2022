// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Data extends DataClass implements Insertable<Data> {
  final int dataid;
  final String name;
  Data({required this.dataid, required this.name});
  factory Data.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return Data(
      dataid: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}dataid'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['dataid'] = Variable<int>(dataid);
    map['name'] = Variable<String>(name);
    return map;
  }

  DataCompanion toCompanion(bool nullToAbsent) {
    return DataCompanion(
      dataid: Value(dataid),
      name: Value(name),
    );
  }

  factory Data.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Data(
      dataid: serializer.fromJson<int>(json['dataid']),
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'dataid': serializer.toJson<int>(dataid),
      'name': serializer.toJson<String>(name),
    };
  }

  Data copyWith({int? dataid, String? name}) => Data(
        dataid: dataid ?? this.dataid,
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('Data(')
          ..write('dataid: $dataid, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(dataid, name);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Data && other.dataid == this.dataid && other.name == this.name);
}

class DataCompanion extends UpdateCompanion<Data> {
  final Value<int> dataid;
  final Value<String> name;
  const DataCompanion({
    this.dataid = const Value.absent(),
    this.name = const Value.absent(),
  });
  DataCompanion.insert({
    this.dataid = const Value.absent(),
    required String name,
  }) : name = Value(name);
  static Insertable<Data> custom({
    Expression<int>? dataid,
    Expression<String>? name,
  }) {
    return RawValuesInsertable({
      if (dataid != null) 'dataid': dataid,
      if (name != null) 'name': name,
    });
  }

  DataCompanion copyWith({Value<int>? dataid, Value<String>? name}) {
    return DataCompanion(
      dataid: dataid ?? this.dataid,
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (dataid.present) {
      map['dataid'] = Variable<int>(dataid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DataCompanion(')
          ..write('dataid: $dataid, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }
}

class DataTable extends Table with TableInfo<DataTable, Data> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  DataTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _dataidMeta = const VerificationMeta('dataid');
  late final GeneratedColumn<int?> dataid = GeneratedColumn<int?>(
      'dataid', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      $customConstraints: 'NOT NULL PRIMARY KEY');
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  late final GeneratedColumn<String?> name = GeneratedColumn<String?>(
      'name', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  @override
  List<GeneratedColumn> get $columns => [dataid, name];
  @override
  String get aliasedName => _alias ?? 'Data';
  @override
  String get actualTableName => 'Data';
  @override
  VerificationContext validateIntegrity(Insertable<Data> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('dataid')) {
      context.handle(_dataidMeta,
          dataid.isAcceptableOrUnknown(data['dataid']!, _dataidMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {dataid};
  @override
  Data map(Map<String, dynamic> data, {String? tablePrefix}) {
    return Data.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  DataTable createAlias(String alias) {
    return DataTable(attachedDatabase, alias);
  }

  @override
  bool get dontWriteConstraints => true;
}

abstract class _$AppDb extends GeneratedDatabase {
  _$AppDb(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  late final DataTable data = DataTable(this);
  Selectable<Data> testing() {
    return customSelect('SELECT * FROM Data', variables: [], readsFrom: {
      data,
    }).map(data.mapFromRow);
  }

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [data];
}
