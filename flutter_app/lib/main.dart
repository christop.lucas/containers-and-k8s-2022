import 'package:flutter/material.dart';
import 'package:flutter_app/viewmodel/viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'view/dataview.dart';
import 'services/repository.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp(
    repository: Repository(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key, required this.repository}) : super(key: key);

  final Repository repository;

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Provider.value(
      value: (context) => widget.repository,
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => DataViewModel(repository: widget.repository),
          ),
        ],
        child: MaterialApp(
          theme: ThemeData(
              primarySwatch: Colors.blue,
              visualDensity: VisualDensity.adaptivePlatformDensity),
          home: const DataView(
            title: 'Test',
          ),
        ),
      ),
    );
  }

  @override
  void dispose() async {
    super.dispose();
    final shared = await SharedPreferences.getInstance();
    shared.setString("lastReadData", DateTime.now().toIso8601String());
  }
}
