import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../services/database/database.dart';
import '../viewmodel/viewmodel.dart';

class DataView extends StatefulWidget {
  const DataView({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  _DataViewState createState() => _DataViewState();
}

class _DataViewState extends State<DataView> {
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final dataViewModel = context.watch<DataViewModel>();
    switch (dataViewModel.status) {
      case DataStatus.loading:
        dataViewModel.fetchData();
        return const Waiting();
      case DataStatus.success:
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
            actions: [
              IconButton(
                onPressed: (() {
                  dataViewModel.fetchData();
                }),
                icon: const Icon(Icons.refresh),
              ),
            ],
          ),
          body: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Form(
                  child: TextFormField(
                    controller: _controller,
                    decoration:
                        const InputDecoration(labelText: 'Send a message'),
                  ),
                ),
                const SizedBox(height: 24),
                StreamBuilder(
                  stream: dataViewModel.showData(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData && snapshot.data != null) {
                      var data = snapshot.data as List<Data>;
                      return Expanded(
                        child: ListView.builder(
                          itemCount: data.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              title: Text(data[index].name),
                            );
                          },
                        ),
                      );
                    } else {
                      return const Text("data");
                    }
                  },
                ),
                /*Consumer<DataViewModel>(
                  builder: (context, data, child) {
                    return Expanded(
                      child: ListView.builder(
                        itemCount: data.datas.length,
                        itemBuilder: (context, index) {
                          return ListTile(
                            title: Text(data.datas[index].name),
                          );
                        },
                      ),
                    );
                  },
                ),*/
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              if (_controller.text.isNotEmpty) {
                //_channel.sink.add(_controller.text);
                dataViewModel.insertData(
                  Data(
                    dataid: -1,
                    name: _controller.text,
                  ),
                );
                _controller.clear();
              }
            }, //,
            tooltip: 'Send message',
            child: const Icon(Icons.send),
          ), // This trailing comma makes auto-formatting nicer for build methods.
        );
      case DataStatus.failure:
        return const Text("data");
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class Waiting extends StatelessWidget {
  const Waiting({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
