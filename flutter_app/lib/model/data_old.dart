class DataOld {
  int dataid;
  String name;

  DataOld({
    required this.dataid,
    required this.name,
  });

  DataOld.fromJson(Map<String, dynamic> json)
      : dataid = json['dataid'],
        name = json['name'];

  Map<String, dynamic> toJson() => {
        'dataid': dataid,
        'name': name,
      };

  Map<String, dynamic> toSqlite() => {
        'dataid': dataid,
        'name': name,
      };

  DataOld.fromSqlite(Map<String, dynamic> data)
      : dataid = data['dataid'],
        name = data['name'];

  static final empty = DataOld(
    dataid: -1,
    name: "unkown",
  );
}
