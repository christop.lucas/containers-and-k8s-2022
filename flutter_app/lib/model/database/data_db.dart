import 'package:drift/drift.dart';

@DataClassName('DataDB')
class DataDB extends Table {
  IntColumn get dataid => integer()();
  TextColumn get data => text()();
}
