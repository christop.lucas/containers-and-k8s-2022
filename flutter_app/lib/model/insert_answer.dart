class InsertAnswer {
  String result;
  int id;

  InsertAnswer({
    required this.result,
    required this.id,
  });

  InsertAnswer.fromJson(Map<String, dynamic> json)
      : result = json['result'],
        id = json['id'];

  Map<String, dynamic> toJson() => {
        'result': result,
        'id': id,
      };
}
