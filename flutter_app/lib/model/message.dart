class Message {
  List<int> ids;
  String type;

  Message({
    required this.ids,
    required this.type,
  });

  Message.fromJson(Map<String, dynamic> json)
      : ids = json['ids'].cast<int>(),
        type = json['type'];

  Map<String, dynamic> toJson() => {
        'ids': ids,
        'type': type,
      };
}
